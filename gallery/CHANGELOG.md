# 0.1.2

+ Added carouselItemBackground color

# 0.1.1

+ Updated file names to comply with Dart naming standards (only letters, underscores, etc.)

# 0.1.0

+ Initial Release